// Copyright 2018 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'package:flutter/material.dart';
import 'package:unit_converter/category.dart';

// TODO: Check if we need to import anything

// TODO: Define any constants

/// Category Route (screen).
///
/// This is the 'home' screen of the Unit Converter. It shows a header and
/// a list of [Categories].
///
/// While it is named CategoryRoute, a more apt name would be CategoryScreen,
/// because it is responsible for the UI at the route's destination.
class CategoryRoute extends StatelessWidget {
  const CategoryRoute();

  static const _categoryNames = <String>[
    'Length',
    'Area',
    'Volume',
    'Mass',
    'Time',
    'Digital Storage',
    'Energy',
    'Currency',
  ];

  static const _baseColors = <Color>[
    Colors.teal,
    Colors.orange,
    Colors.pinkAccent,
    Colors.blueAccent,
    Colors.yellow,
    Colors.greenAccent,
    Colors.purpleAccent,
    Colors.red,
  ];

  static const _baseIcon = Icons.subscriptions;
  static const _padding = 8.0;

  @override
  Widget build(BuildContext context) {
    // TODO: Create a list of the eight Categories, using the names and colors
    // from above. Use a placeholder icon, such as `Icons.cake` for each
    // Category. We'll add custom icons later.

    // TODO: Create a list view of the Categories
    final listView = Container(
        child: ListView.builder(
          physics: BouncingScrollPhysics(),
      itemCount: _categoryNames.length,
      padding: EdgeInsets.all(_padding),
      itemBuilder: (context, index) {
        return Category(
            name: _categoryNames[index],
            icon: _baseIcon,
            color: _baseColors[index]);
      },
    ));

//    final listView = NotificationListener<OverscrollIndicatorNotification>(
//      onNotification: (overscroll) {
//        overscroll.disallowGlow();
//      },
//      child: new ListView.builder(
//        physics: ClampingScrollPhysics(),
//        itemCount: _categoryNames.length,
//        padding: EdgeInsets.all(_padding),
//        itemBuilder: (context, index) {
//          return Category(
//              name: _categoryNames[index],
//              icon: _baseIcon,
//              color: _baseColors[index]);
//        },
//      ),
//    );

    // TODO: Create an App Bar
    final appBar = AppBar(
      centerTitle: true,
      title: Text(
        "Unit Converter",
        textAlign: TextAlign.center,
        style: TextStyle(fontSize: 30.0, color: Colors.black),
      ),
      elevation: 0.0,
      backgroundColor: Colors.green[100],
    );

    return Scaffold(
      appBar: appBar,
      body: listView,
      backgroundColor: Colors.green[100],
    );
  }
}
